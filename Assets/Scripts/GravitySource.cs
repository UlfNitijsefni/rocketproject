using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class GravitySource : MonoBehaviour
{
    public Rigidbody rb;
    
    //Use to manipulate gravity force, currently is 10^12 more powerful than IRL
    private float _gravitationalConstant = (float) 66.74;

    private void FixedUpdate()
    {
        Rigidbody[] AttractedObject = FindObjectsOfType<Rigidbody>();

        foreach (var celestialBody in AttractedObject)
        {
            if (celestialBody != this.rb)
            {
                Attract(celestialBody);
            }
        }
    }


    public void Attract(Rigidbody attractedObject)
    {
        Vector3 direction = rb.position - attractedObject.position;
        float distance = direction.magnitude;
        
        float forceMagnitude = _gravitationalConstant * (rb.mass * attractedObject.mass) / Mathf.Pow(distance, 2);
        
        Vector3 force = direction.normalized * forceMagnitude;
        
        attractedObject.AddForce(force);
    }
}
