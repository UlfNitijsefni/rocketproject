using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AsteroidSpawner : MonoBehaviour
{
    private float _gravitationalConstant = (float) 66.74;
    
    public GameObject spherePrefab;
    public int sphereDensity;
    public int seed;
    public float innerRadius, outerRadius;
    public int height;
    public bool rotatingClockwise;
    public int asteroidScale = 1;

    public float minOrbitSpeed, maxOrbitSpeed;
    public float minRotationSpeed, maxRotationSpeed;

    private Vector3 _localPosition, _worldOffset, _worldPosition;
    private float _randomRadius, _randomRadian;
    private float _x, _y, _z;
    
    
    
    // x = cx + r* cos(a)
    // y = cy + r* sin(a)

     private void Start()
     {
         Random.InitState(seed);
         
         for (int i = 0; i < sphereDensity; i++)
         {
             do
             {
                 _randomRadius = Random.Range(innerRadius, outerRadius);
                 _randomRadian = Random.Range(0, (2 * Mathf.PI));
         
                 _y = Random.Range(-(height / 2), height / 2);
                 _x = _randomRadius * Mathf.Cos(_randomRadian);
                 _z = _randomRadius * Mathf.Sin(_randomRadian);
         
             } while (float.IsNaN(_z) && float.IsNaN(_x));
             
             _localPosition = new Vector3(_x, _y ,_z); 
             _worldOffset = transform.rotation * _localPosition;
             _worldPosition = transform.position + _worldOffset;

             var tempSpeed = (float)Math.Sqrt(_gravitationalConstant*transform.GetComponent<Rigidbody>().mass / _randomRadius);
             
             GameObject _asteroid = Instantiate(spherePrefab, _worldPosition, Quaternion.Euler(Random.Range(0, 360),
                 Random.Range(0, 360), Random.Range(0, 360)));
             
             _asteroid.AddComponent<AsteroidObject>().SetUpAsteroid(tempSpeed,
                 Random.Range(minRotationSpeed, maxRotationSpeed), true, gameObject);

             var _straightDown = (transform.position - _worldPosition).normalized;

             _asteroid.GetComponent<Rigidbody>().velocity = tempSpeed * Vector3.Cross(_straightDown, transform.up);
             
             _asteroid.transform.localScale = new Vector3(asteroidScale*(1 + Random.Range(-1/10, 10)), asteroidScale*(1 + Random.Range(-1/10, 10)), asteroidScale*(1 + Random.Range(-1/10, 10)));
             _asteroid.transform.SetParent(transform);
         }
     }
}
