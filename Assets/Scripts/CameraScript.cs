using System;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField] private Transform _targetObject;
    [SerializeField] private AnimationCurve _zoomCurve;
    [SerializeField] private float _changeTime;
    [SerializeField] private AnimationCurve _cameraRotationCurve;

    private Vector3 _newPosition;
    private Vector3 _newRotation;

    private void Start()
    {
        CalculatePositionAndRotation();
    
        transform.localPosition = _newPosition;
        transform.localRotation = Quaternion.Euler(_newRotation);
    }

    private void Update()
    {
        CalculatePositionAndRotation();
    }

    private void FixedUpdate()
    {
        transform.localPosition = Vector3.Lerp(transform.localPosition, _newPosition, Time.fixedDeltaTime);
        // transform.localPosition = Vector3.Lerp(transform.localPosition, _newPosition, Time.fixedDeltaTime / _changeTime);
        transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(_newRotation), Time.fixedDeltaTime);
    }

    private void CalculatePositionAndRotation()
    {
        Vector3 angularV = transform.InverseTransformDirection(transform.parent.GetComponent<Rigidbody>().angularVelocity);
        // Vector3 angularV = transform.parent.GetComponent<Rigidbody>().angularVelocity;
        // Debug.Log(angularV);
        
        _newPosition = new Vector3(3 - _cameraRotationCurve.Evaluate(angularV.x)/9, _zoomCurve.Evaluate(transform.parent.GetComponent<Rigidbody>().velocity.magnitude), 0 - _cameraRotationCurve.Evaluate(angularV.y)/5);
        
        
        // Debug.Log(angularV);
        // Debug.Log(_tiltWithShipSpinCurve.Evaluate(angularV.x));
        
        _newRotation = new Vector3(90, 90 + _cameraRotationCurve.Evaluate(angularV.z/3), 0);
        // _newRotation = new Vector3(90 + _cameraRotationCurve.Evaluate(angularV.x)/3, 90 + _cameraRotationCurve.Evaluate(angularV.y), _cameraRotationCurve.Evaluate(angularV.z));
    }
}