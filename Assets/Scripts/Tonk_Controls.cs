using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Tonk_Controls : MonoBehaviour
{

    [SerializeField] private Vector3 currentGravity;
    public void Start()
    {
        Physics.gravity = currentGravity;
    }

    public List<SteeringAxleInfo> SteeringAxleInfos;
    public List<NonSteeringAxleInfo> NonSteeringAxleInfos;
    public float maxMotorTorque;
    public float maxSteeringAngle;
     
    public void ApplyLocalPositionToVisuals(WheelCollider collider){
        if(collider.transform.childCount == 0){
            return;
        }
        Transform visualWheel = collider.transform.GetChild(0);
        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);
         
        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }
 
    public void FixedUpdate()
    {
        //Strange, but is left after ship setting
        float motor = maxMotorTorque * Input.GetAxis("Depth");

        if (motor != 0)
        {
            print("Motor detected");
        }

        float steering = maxSteeringAngle * Input.GetAxis("Horizontal");
        
        if (steering != 0)
        {
            print("Steering detected");
        }
        
        foreach (SteeringAxleInfo SteeringAxleInfo in SteeringAxleInfos)
        {
            if (SteeringAxleInfo.steering)
            {
                SteeringAxleInfo.leftFrontWheel.steerAngle = steering;
                SteeringAxleInfo.rightFrontWheel.steerAngle = steering;
                SteeringAxleInfo.leftBackWheel.steerAngle = -steering;
                SteeringAxleInfo.rightBackWheel.steerAngle = -steering;
            }

            if (SteeringAxleInfo.motor)
            {
                SteeringAxleInfo.leftFrontWheel.motorTorque = motor;
                SteeringAxleInfo.rightFrontWheel.motorTorque = motor;
                SteeringAxleInfo.leftBackWheel.motorTorque = motor;
                SteeringAxleInfo.rightBackWheel.motorTorque = motor;
            }
            
             
            ApplyLocalPositionToVisuals(SteeringAxleInfo.leftFrontWheel);
            ApplyLocalPositionToVisuals(SteeringAxleInfo.rightFrontWheel);
            
            ApplyLocalPositionToVisuals(SteeringAxleInfo.leftBackWheel);
            ApplyLocalPositionToVisuals(SteeringAxleInfo.rightBackWheel);
        }

        foreach(NonSteeringAxleInfo NonSteeringAxleInfo in NonSteeringAxleInfos){
            if (NonSteeringAxleInfo.motor)
            {
                NonSteeringAxleInfo.leftMidWheel.motorTorque = motor;
                NonSteeringAxleInfo.rightMidWheel.motorTorque = motor;
            }

            ApplyLocalPositionToVisuals(NonSteeringAxleInfo.leftMidWheel);
            ApplyLocalPositionToVisuals(NonSteeringAxleInfo.rightMidWheel);
        }
    }
}
 
[System.Serializable]
public class SteeringAxleInfo{
    public WheelCollider leftFrontWheel;
    public WheelCollider rightFrontWheel;
    public WheelCollider leftBackWheel;
    public WheelCollider rightBackWheel;
    public bool motor;
    public bool steering;
}

[System.Serializable]
public class NonSteeringAxleInfo{
    public WheelCollider leftMidWheel;
    public WheelCollider rightMidWheel;
    public bool motor;
}