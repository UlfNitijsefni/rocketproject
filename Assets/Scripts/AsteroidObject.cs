using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AsteroidObject : MonoBehaviour
{
    [SerializeField]
    private float orbitSpeed;
    [SerializeField]
    private GameObject parent;
    [SerializeField]
    private bool rotatingClockwise;
    [SerializeField]
    private float rotationSpeed;
    [SerializeField]
    private Vector3 rotationDirection;
    [SerializeField] 
    private Vector3 scale;
    
    
    public void SetUpAsteroid(float _speed, float _rotationSpeed, bool _rotatingClockwise, GameObject _parent)
    {
        orbitSpeed = _speed;
        rotationSpeed = _rotationSpeed;
        rotatingClockwise = _rotatingClockwise;
        parent = _parent;
        float tempx = Random.Range(0, 360F);
        float tempy = Random.Range(0, 360F);
        float tempz = Random.Range(0, 360F);

        float tempXScale = Random.Range(5, 15);
        float tempYScale = Random.Range(5, 15);
        float tempZScale = Random.Range(5, 15);
        rotationDirection = new Vector3(tempx,tempy,tempz);
        gameObject.transform.localScale = new Vector3(tempXScale, tempYScale, tempZScale);
    }

    private void Update()
    {
    // if (rotatingClockwise)
    // {
    //     transform.RotateAround(parent.transform.position, parent.transform.up, orbitSpeed * Time.deltaTime);
    // }
    // else
    // {
    //     transform.RotateAround(parent.transform.position, -parent.transform.up, orbitSpeed * Time.deltaTime);
    // }
    //     
        transform.Rotate(rotationDirection, rotationSpeed * Time.deltaTime);
    }
}
