using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{

    [SerializeField] private float xSpeed;
    [SerializeField] private float ySpeed;
    [SerializeField] private float zSpeed;
    [SerializeField] private float enginePower;

    private InputMaster _inputMaster;
    private Vector3 startingPosition;
    private Quaternion startingRotation;

    private void Start()
    {
        _inputMaster = new InputMaster();
        _inputMaster.Enable();
        startingPosition = gameObject.transform.position;
        startingRotation = gameObject.transform.rotation;
    }

    public KeyCode thrust, retrieval, zeroRotation, zeroSpeed;
    private Vector3 _inputRoll;

    private void OnDestroy()
    {
        _inputMaster.Dispose();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        
        //var input = _inputSystem.NazwaMapy.NazwaAkcji.ReadValue(Vector2);
        //var input = inputSystem.nazwamapy.nazwaPrzycisku.tirggered
        
        Rigidbody player = GetComponent<Rigidbody>();

        // var pitchAndYawInput = _inputMaster.Ship.YawAndPitch.ReadValue<Vector2>();
        // var rollInput = _inputMaster.Ship.Roll.ReadValue<float>();
        
        // Debug.Log(pitchAndYawInput);
        // Debug.Log(rollInput);
        
        
        var playerMass = player.mass;
        xSpeed = 100 * xSpeed / playerMass;
        ySpeed = 100 * ySpeed / playerMass;
        zSpeed = 100 * zSpeed / playerMass;

        // return;
        
        if (Input.GetKey(thrust))
        {
            ApplyThrust(0, -enginePower, 0, player);
        }
        else if (Input.GetKey(retrieval))
        {
            transform.position = (startingPosition);
            transform.rotation = Quaternion.Euler(0, 0, 0);
            player.velocity = new Vector3(0, 0, 0);
            player.angularVelocity = new Vector3(0, 0, 0);
            player.rotation = startingRotation;
        }
        else if (Input.GetKey(zeroRotation))
        {
            player.angularVelocity = new Vector3(0,0,0);
        }
        
        else if (Input.GetKey(zeroSpeed))
            player.velocity = new Vector3(0, 0, 0);

        else
        {
            _inputRoll = new Vector3(Input.GetAxis("Horizontal") * xSpeed, Input.GetAxis("Vertical") * ySpeed, Input.GetAxis("Depth") * zSpeed);
            
            player.AddRelativeTorque(_inputRoll * Time.deltaTime);
        }
    }

    private void ApplyThrust(float x, float y, float z, Rigidbody objectPropelled)
    {
        objectPropelled.AddRelativeForce(x, y, z);
    }
}
