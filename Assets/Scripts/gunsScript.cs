using System;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem.LowLevel;
using Object = UnityEngine.Object;

public class gunsScript : MonoBehaviour
{
    [SerializeField] float gunPower = 10;
    [SerializeField] private int range = 10000;
    [SerializeField] private Transform shipsGun;
    [SerializeField] private ParticleSystem muzzleFlash;
    // [SerializeField] private GameObject impactEffect;
    [SerializeField] private float impactForce = 10f;
    [SerializeField] private int firerate = 20;
    private int hitTriangleIndex; 
    private LineRenderer laserLine;


    private float lastFired = 0;

    private void Awake()
    {
        laserLine = transform.GetComponent<LineRenderer>();
    }

    void Update()
    {
        //TODO: Make sure there are hitmarkers and make it full auto, it's single fire only
        //TODO: For some reason lines stay until next shot
        if (Input.GetButton("Mouse") && Time.time >= lastFired)
        {
            laserLine.SetPosition(0, shipsGun.position);
            if (Time.time - lastFired > 1 / firerate)
            {
                lastFired = Time.time;
                Shoot();
            }
        }
    }

    void Shoot()
    {
        muzzleFlash.Play();
        laserLine.SetPosition(0, shipsGun.position);
        Vector3 rayOrigin = shipsGun.position;
        
        //TODO: Add equal and oppostie reaction to the gun itself
        if (Physics.Raycast(shipsGun.position, shipsGun.forward, out var hit, range))
        {
            laserLine.SetPosition(1, hit.point);

            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce((hit.point - shipsGun.position) * impactForce);
                // hit.rigidbody.AddForce(-hit.normal * impactForce);
                
                
                // hitTriangleIndex = hit.triangleIndex;
                
                // Debug.Log(hitTriangleIndex);
                
                // MeshCollider hitCollider = hit.collider as MeshCollider;
                // Mesh currentMesh = hitCollider.sharedMesh;
                // var triangles = currentMesh.triangles.ToList();
                // triangles.RemoveAt(hitTriangleIndex);
                // triangles.RemoveAt(hitTriangleIndex);
                // triangles.RemoveAt(hitTriangleIndex);
            }
        }
        else
        {
            laserLine.SetPosition(1, rayOrigin + shipsGun.transform.forward*range);
        }
    }
}
